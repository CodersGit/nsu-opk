#include <stdio.h>

int main() {
    int day = 0, month = 0, year = 0;
    printf("Input day, month, year: ");
    scanf ("%d %d %d", &day, &month, &year);
    printf("Day number is - %d", day_number(day, month, year));
    return 0;
}

int day_number (int day, int month, int year) {
    if (month > 12 || month < 1 || day < 1)  {
        printf("Are you OK? o_O\n");
        return -1;
    }
    int days[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
    int daysV[12] = {31,29,31,30,31,30,31,31,30,31,30,31};
    int v;
    if ((year % 100 != 0 && year % 4 == 0) || year % 400 == 0) v = 1;
        else  v = 0;
    if (v) {
        if (day > daysV[month - 1]) {
            printf("There isn't so much days in this month! 8)\n");
            return -1;
        }
    } else {
        if (day > days[month - 1]) {
            printf("There isn't so much days in this month! 8)\n");
            return -1;
        }
    }
    for (int i = 1; i < month; i++)
        if (v) day += daysV[i - 1]; else day += days[i - 1];
    return day;
}
