#ifndef ARRAY_ANALYSIS_H_INCLUDED
#define ARRAY_ANALYSIS_H_INCLUDED

float get_min(float *a, int n);
float get_max(float *a, int n);
float get_average(float *a, int n);
float get_median(float *a, int n);
float get_std_deviation (float *a, int n);
unsigned int get_max_quantity_consecutive_equal_elems (float *a, int n);
unsigned int get_mqcee (float *a, int n);
unsigned int get_max_monotony_lengh (float *a, int n);
unsigned int get_mml (float *a, int n);

#endif // ARRAY_ANALYSIS_H_INCLUDED
