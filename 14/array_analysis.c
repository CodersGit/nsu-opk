#include "array_analysis.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>
#include "sort.h"

float get_min(float *a, int n) {
	float min = a[0];
	for(int i = 1; i < n; i++)
		if (min > a[i])
			min = a[i];
	return min;
}

float get_max(float *a, int n) {
	float max = a[0];
	for(int i = 1; i < n; i++)
		if (max < a[i])
			max = a[i];
	return max;
}

float get_average(float *a, int n) {
	float summ = 0;
	for(int i = 0; i < n; i++)
		summ += a[i];
	return summ / n;
}

int compare (const void *a, const void *b) {
//	printf("===%d===\n", *((float *)a) < *((float *)b));
	return *((float *)a) < *((float *)b);
}

float get_median(float *a, int n) {
	float temp[n];
	for(int i = 0; i < n; i++)
		temp[i] = a[i];
	mergesort(temp, n, sizeof(float), compare);
//    for(int i = 0; i < n; i++)
//		printf ("%f\n", temp[i]);
	for(int i = 1; i < n; i++)
		assert(temp[i - 1] <= temp[i]);
	return (n % 2 == 0)? (temp[n/2] + temp[(n - 1)/2])/2 : temp[(n - 1)/2];
}

float get_std_deviation (float *a, int n) {
	float a_average = get_average(a, n), sum = 0;
	for(int i = 0; i < n; i++) {
		sum += pow(a[i] - a_average, 2);
	}
	return sqrt(sum/(n - 1));
}

unsigned int get_max_quantity_consecutive_equal_elems (float *a, int n) {
	unsigned int max = 1, temp = 1;
	for(int i = 0; i < n; i++) {
		if(a[i - 1] == a[i]) {
			temp++;
		} else {
			if(temp > max) max = temp;
			temp = 1;
		}
	}
	return max;
}

unsigned int get_mqcee (float *a, int n) {
	return get_max_quantity_consecutive_equal_elems(a, n);
}

unsigned int get_max_monotony_lengh (float *a, int n) {
	unsigned short int type = 0, tmp_type;
	unsigned int max = 1, temp = 1;
	for(int i = 1; i < n; i++) {
		tmp_type = (a[i - 1] < a[i])? 1 : 2;
		if (tmp_type == type) {
			temp++;
		} else {
			type = tmp_type;
			if(temp > max) max = temp;
			temp = 1;
		}
	}
	return max;
}

unsigned int get_mml (float *a, int n) {
	return get_max_monotony_lengh(a, n);
}
