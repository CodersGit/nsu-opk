#ifndef SORT_H_INCLUDED
#define SORT_H_INCLUDED

void mergesort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));

#endif // SORT_H_INCLUDED
