#include <stdio.h>
#include "array_analysis.h"

void scanf_float_crutch (float *var) { // ������� ��� ������ float
    int i, j = 0, k;
    scanf("%d.%d", &i, &j);
    for(k = 1; j =! 0 && j%k == 0; k *= 10);
    if (i >= 0)
        *var = i + (float) j / k;
    else
        *var = i - (float) j / k;
}

int main () {
    printf("Input number of elements: ");
    int n;
    scanf("%d", &n);
    float array[n];
    for(int i = 0; i < n; i++) {
        printf("Input %d th number: ", i + 1);
        scanf_float_crutch(array + i);
    }
    printf("Min:\t\t\t\t\t\t%f\n", get_min(array, n));
    printf("Max:\t\t\t\t\t\t%f\n", get_max(array, n));
    printf("Average:\t\t\t\t\t%f\n", get_average(array, n));
    printf("Median:\t\t\t\t\t\t%f\n", get_median(array, n));
    printf("Standart deviation:\t\t\t\t%f\n", get_std_deviation(array, n));
    printf("Max quantity consecutive equal elems:\t\t%d\n", get_mqcee(array, n));
    printf("Max monotony lengh:\t\t\t\t%d\n", get_mml(array, n));
    for(int i = 0; i < n; i++)
		printf ("%f\n", array[i]);
    return 0;
}
