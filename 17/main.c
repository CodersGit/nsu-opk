#include <stdio.h>
#define MAX_COUNT 100000

int main() {
    int count = 0, num;
    scanf("%d", &count);
    num = takefibonacchi(count);
    printf("That's %lld", num);
    return 0;
}
int takefibonacchi (int count) {
    long long int a[MAX_COUNT];
    a[0] = 0;
    a[1] = 1;
    if (count >= MAX_COUNT){
        printf("Sorry, number is too large");
        return -1;
    }
    for(int i = 2; i <= count; i++)
        a[i] = a[i - 1] + a[i - 2];
    return a[count];
}
