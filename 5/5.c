#include <stdio.h>
#include <stdbool.h>
#define MAX_NUM 100000

int simplenums[MAX_NUM];
int simplecount = 0;

int main () {
    int maxnum;
    printf("Input max number");
    if (!scanf("%d", &maxnum) || maxnum >= MAX_NUM) {
        printf("Incorrect input!\n\n");
        return 0;
    }
    printf("\t");
    for(int i = 2; i <= maxnum; i++) {
        if(CheckSimple(i))
            printf("%d", i);
    }
    return 0;
}

int CheckSimple(int number) {
    int i;
    for(i = 0; i<simplecount; i++)
        if(number % simplenums[i] == 0)
            return false;
    printf(" %d\n\t", i);
    simplenums[simplecount] = number;
    simplecount++;
    if(simplecount >= 100000000) {
        printf("Too many simple numbers!");
        return false;
    }
    return true;
}
