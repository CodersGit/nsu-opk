#include <stdio.h>

int main() {
    int a, b;
    scanf("%d %d", &a, &b);
    printf("%d", greatest_common_divisor(a, b));
    return 0;
}
int greatest_common_divisor (int a, int b) {
    if (a == 0)
        return b;
    else if (b == 0)
        return a;
    else if (a > b)
        return greatest_common_divisor(a%b, b);
    else
        return greatest_common_divisor(a, b%a);
}
