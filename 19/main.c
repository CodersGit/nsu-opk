#include <stdio.h>
#include <math.h>

double polynom (double x, double a[], int n) {
    if (x == 0)
        return *a;
    double result = 0;
    for (int i = n; i >= 0; i--)
        result += (pow(x,i)) * a[i];
    return result;
}
int main() {
    printf("Input number of parts: ");
    int n = 0;
    double x = 0;
    scanf("%d", &n);
    if (n <= 0) {
        printf("Incorrect input!");
        return 0;
    }
    printf("Input coefficients:\n");
    double a[n];
    for(int i = n - 1; i >= 0; i--) {
        printf("a%d: ", n - i);
        scanf("%lf", a + i);
    }
    for(int i = 0; i < n;i++)
        printf("%lf, ", a[i]);
    printf("Input x: ");
    scanf("%lf", &x);
    printf("answer is %lf", polynom(x, a, n));
    return 0;
}
