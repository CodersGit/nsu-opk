#include <stdio.h>
#include <string.h>

char find_symbol_for_rle (FILE *input) {
	int symbs[256], symbol = 0, symb_q;
	for (int i = 0; i < 256; i++)
		symbs[i] = 0;
	while ((symbol = fgetc(input)) != EOF)
		symbs[symbol]++;
	symb_q = symbs[symbol = 0];
	for (int i = 0; i < 256; i++) {
		if (symbs[i] == 0)
			return i;
		else if (symb_q < symbs[i]) {
			symb_q = symbs[i];
			symbol = i;
		}
	}
	return symbol;
}

void rle_compress (FILE *input, FILE *output, char symbol) {
//	 = 0;
	unsigned char count = 0;
	int c, prev = EOF;
	fputc(symbol, output);
	while((c = fgetc(input)) != EOF) {

		if (c == prev && count < 255) {
			count++;
		} else {
			if(count <= 3 && prev != symbol) {
				for(int i = 1; i <= count; i++)
					fputc(prev, output);
			} else {
				fputc(symbol, output);
				fputc(count, output);
				fputc (prev, output);
			}
			prev  = c;
			count = 1;
		}
	}
	if (count <= 3)
		for(int i = 1; i <= count; i++)
			fputc(prev, output);
	else {
		fputc(symbol, output);
		fputc(count, output);
		fputc (prev, output);
	}
}

void rle_decompress (FILE *input, FILE *output) {
	unsigned char count = 0, step = 0;
	int c;
	char symbol = 0;
	c = fgetc(input);
	if(c == EOF) {
		printf("Input file is empty!");
		return;
	}
	symbol = c;
	while((c = fgetc(input)) != EOF) {
		switch (step) {
		default:
		case 0:
			if (c == symbol)
				step++;
			else
				fputc(c, output);
			break;
		case 1:
			count = c;
			step++;
			break;
		case 2:
			for(int i = 1; i <= count; i++)
				fputc(c, output);
			step = 0;
			break;
		}
	}
}

int main(int argc, char * argv[]) {
	if(!argc || argc != 4) {
		printf("Incorrect arguments quantity!\n");
		return 1;
	}
	if(strcmp(argv[1], "c") && strcmp(argv[1], "d")) {
		printf("Incorrect action: %s\n", argv[1]);
		return 2;
	}
	if(!strcmp(argv[2], argv[3])) {
		printf("Input file can't be equal with output file!\n");
		return 3;
	}
	FILE *input;
	input  = fopen(argv[2], "r");
	if(input == NULL) {
		printf("Can\'t open file: %s\n", argv[2]);
		return 4;
	}
	FILE *output;
	output = fopen(argv[3], "w");
	if(output == NULL) {
		fclose(input);
		printf("Can\'t open file: %s\n", argv[3]);
		return 5;
	}
	if (!strcmp(argv[1], "c")) {
		char symbol = find_symbol_for_rle(input);
		fclose(input);
		input  = fopen(argv[2], "r");
		rle_compress(input, output, symbol);
	} else
		rle_decompress(input, output);

	fclose (input);
	fclose(output);
    return 0;
}
