#include <stdio.h>
#include <assert.h>
#include "array.h"

int main() {
    void *array = ArrayCreate();
    assert (array);
    for (long i = 0; i <= 512; i++)
        assert (ArrayAppen(array, i));
    long a;
    assert(ArrayGet(array, 300, &a)); //����� �� 300
    assert(a == 300);
    int size;
    assert(ArrayGetSize(array, &size));
    printf("%d %d\n", size, a);
    ArrayDelete(array);
    return 0;
}
