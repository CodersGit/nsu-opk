#include <stdlib.h>
#define INITIAL_CAPACITY 256

struct DynArray {
    int size;
    int capacity;
    long *array;
} DynArr_t;

struct DynArray * ArrayCreate () {
    struct DynArray *A = (struct DynArray *) malloc(sizeof(struct DynArray));
    if(!A) {
        free(A);
        return NULL;
    }
    A->array = (long *) calloc(INITIAL_CAPACITY, sizeof(long));
    A->capacity = INITIAL_CAPACITY;
    if(!A->array)
        A->capacity = 0;
    else
        A->capacity = INITIAL_CAPACITY;
    A->size = 0;
    return A;
}

void ArrayDelete (struct DynArray *A) {
    free(A->array);
    A->capacity = 0;
    A->size = 0;
    free(A);
    A = NULL;
    return A;
}

int ArrayAppen (struct DynArray *A, long elem) {
    if (!A)
        return NULL;
    if (A->size + 1 >= A->capacity) {
        long *new_array = realloc(A->array, A->capacity*2*sizeof(long));
        if(!new_array) return NULL;
        A->array = new_array;
        A->capacity *= 2;
    }
    A->array[A->size] = elem;
    A->size++;
    return 1;
}

int ArrayGet (struct DynArray *A, int elem, long * item) {
    if (!A)
        return NULL;
    if (elem >= A->size) return NULL;
    *item = *(A->array + elem);
    return 1;
}

int ArrayGetSize (struct DynArray *A, int * var) {
    if (!A)
        return NULL;
    *var = A->size;
    return 1;
}
