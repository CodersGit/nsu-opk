#include <malloc.h>
#include <stdio.h>
#include <mem.h>
#include <assert.h>
#include "sort.h"
#define SHIFT(ptr,i)((void*)(char*)ptr+size*i)

void mergesort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *)) {
	assert (nmemb >= 1);
	if (nmemb == 1) return;
	mergesort(base, (nmemb - nmemb/2), size, compar);
	mergesort(SHIFT(base, (nmemb - nmemb/2)), nmemb/2, size, compar);
	size_t tmp = 0, tmp_2 = nmemb - nmemb/2;
	void *sorted = malloc(nmemb*size);
	for (size_t i = 0; i < nmemb; i++) {
		if(tmp < nmemb - nmemb/2 && (tmp_2 >= nmemb || compar(SHIFT(base, tmp) , SHIFT(base, tmp_2))== 1)) {
			memcpy(SHIFT(sorted, i), SHIFT(base, tmp), size);
//			printf("%f %d\n", *(float *)SHIFT(sorted, i), i);
			tmp++;
		} else if(tmp_2 < nmemb) {
			memcpy(SHIFT(sorted, i), SHIFT(base, tmp_2), size);
//			printf("%f %d\n", *(float *)SHIFT(sorted, i), i);
			tmp_2++;
		} else {
			printf("Error!%d %d\n", tmp, tmp_2);
			return;
		}
	}
//	printf("==================\n");
	for (size_t i = 0; i < nmemb; i++)
		memcpy(SHIFT(base, i), SHIFT(sorted, i), size);
	free(sorted);
}
