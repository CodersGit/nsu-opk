#include <stdio.h>
#include <assert.h>
#include "sort.h"
#define n 100

int compare (const void *a, const void *b) {
	return *((int *)a) < *((int *)b);
}

int main () {
	int array[n];
	srand(time(NULL));
	for (int i = 0; i < n; i++)
		array[i] = rand() % 100;
	for (int i = 0; i < n; i++)
		printf("%d\n", array[i]);
	printf("\n");
	mergesort(array, n, sizeof(int), compare);
	for (int i = 0; i < n; i++)
		printf("%d\n", array[i]);
	for(int i = 1; i < n; i++)
		assert(array[i - 1] <= array[i]);
    return 0;
}
