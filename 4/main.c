#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void show_2d_array (int array[8][8]) {
    printf("\n");
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++)
            printf("[%d]\t", array[i][j]);
        printf("\n");
    }
}

bool check_desk_complete (int desk[8][8]){
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            if (desk[i][j] == 0) return false;
    return true;
}

bool horse_go (int desk_old[8][8], int x, int y, int step) {
    if(desk_old[x][y] != 0 || x >= 8 || y >= 8 || x < 0 || y < 0) return false;
    int desk[8][8];
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            desk[i][j] = desk_old[i][j];
    desk [x][y] = step;
//    show_2d_array (desk);
    if(horse_go(desk, x + 1, y + 2, step + 1))return true;
    if(horse_go(desk, x + 1, y - 2, step + 1))return true;
    if(horse_go(desk, x - 1, y + 2, step + 1))return true;
    if(horse_go(desk, x - 1, y - 2, step + 1))return true;
    if(horse_go(desk, x + 2, y + 1, step + 1))return true;
    if(horse_go(desk, x - 2, y + 1, step + 1))return true;
    if(horse_go(desk, x + 2, y - 1, step + 1))return true;
    if(horse_go(desk, x - 2, y - 1, step + 1))return true;
    bool check = check_desk_complete(desk);
    if (check) show_2d_array (desk);
    return check;
}

int main() {
    int desk[8][8];
    int x,y;
    for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
            desk[i][j] = 0;
    scanf("%d %d", &x, &y);
    if(horse_go (desk, x - 1, y - 1, 1)) {
        printf("\nMagic complete!");
    } else {
        printf("\nMagic fault!");
    }
    return 0;
}
