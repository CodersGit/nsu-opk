#include <stdlib.h>
#include <malloc.h>
#define DATA_TYPE int

typedef struct ListElem {
    DATA_TYPE Data;
    struct ListElem *next;
    struct ListElem *prev;
} ListItem;

struct ListElem * ListCreate (DATA_TYPE data) {
    struct ListElem *A = (struct ListElem *) malloc(sizeof(struct ListElem));
    if(!A) return NULL;
    A->Data = data;
    A->next = NULL;
    A->prev = NULL;
    return A;
}

struct ListElem * ListGetLast (ListItem *list) {
    if(!list) return NULL;
    while (list->next != NULL)
        list = list->next;
    return list;
};

struct ListElem * ListAppend (ListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    ListItem *tmp_elem = ListGetLast(list);
    tmp_elem->next = ListCreate(data);
    if (!tmp_elem->next) return NULL;
    tmp_elem->next->prev = tmp_elem;
    return list;
}

struct ListElem * ListPrepend (ListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    ListItem *tmp_elem = list;
    tmp_elem->prev = ListCreate(data);
    if (!tmp_elem->prev) return NULL;
    tmp_elem->prev->next = tmp_elem;
    tmp_elem = tmp_elem->prev;
    return tmp_elem;
}

void ListInsert (ListItem *elem, DATA_TYPE data) {
    if(!elem) return;
    ListItem *tmp_elem = ListCreate(data);
    if (!tmp_elem) return;
    tmp_elem->prev = elem;
    if(elem->next != NULL) {
        elem->next->prev = tmp_elem;
        tmp_elem->next = elem->next;
    }
    elem->next = tmp_elem;
    return;
}

DATA_TYPE ListDeleteThisItem (ListItem **elemlink) {
    if(!elemlink) return NULL;
    ListItem *elem = *elemlink;
    if(!elem) return NULL;
    ListItem *new_link = NULL;
    if(!elem) return NULL;
    if (elem->prev != NULL) {
        elem->prev->next = elem->next;
    }
    if (elem->next != NULL) {
        elem->next->prev = elem->prev;
        new_link = elem->next;
    }
    DATA_TYPE data = elem->Data;
    elem->Data = 0;
    elem->next = elem->prev = NULL;
    free(elem);
    *elemlink = new_link;
    return data;
}

DATA_TYPE ListDeleteNextElem (ListItem *elem) {
    if(!elem) return NULL;
    if (elem->next == NULL) return NULL;
    elem = elem->next;
    return ListDeleteThisItem(&elem);
}

struct ListElem * ListRemoveByData (ListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    ListItem *tmp = list;
    if(list->Data == data){
        ListDeleteThisItem(&list);
    } else {
        while (tmp->next != NULL)
            if(tmp->next->Data != data)
                tmp = tmp->next;
            else {
                ListDeleteNextElem(tmp);
                return list;
            }
    }
    return list;
}

struct ListElem * ListRemoveAllByData (ListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    ListItem *tmp = list;
    while (tmp->next != NULL)
        if(tmp->Data != data)
                tmp = tmp->next;
        else {
            ListDeleteThisItem(&tmp);
        }
    return tmp;
}

void ListFree (ListItem *list) {
    if(!list) return;
    while (list->next != NULL) {
        ListItem *tmp = list;
        list = list->next;
        ListDeleteThisItem(&tmp);
    }
}

unsigned int ListLengh (ListItem *list) {
    if(!list) return 0;
    unsigned int l = 1;
    while (list->next != NULL) {
        list = list->next;
        l++;
    }
    return l;
}

struct ListElem * ListReverse (ListItem * list) {
    if(!list) return NULL;
    ListItem *tmp = list->next;
    list->next = list->prev;
    list->prev = tmp;
    while (list->prev != NULL) {
        list = list->prev;
        tmp = list->next;
        list->next = list->prev;
        list->prev = tmp;
    }
    return list;
};

struct ListElem * ListGetCopy (ListItem * list) {
    if(!list) return NULL;
    ListItem *new_list = ListCreate(list->Data);
    ListItem *tmp = new_list;
    while (list->next != NULL) {
        list = list->next;
        ListInsert(tmp, list->Data);
        tmp = tmp->next;
    }
    return new_list;
};

struct ListElem * ListNth (ListItem *list, int n) {
    if(!list) return NULL;
    if (n > 0) {
        int i;
        for(i = 1; i <= n && list->next != NULL; i++)
            list = list->next;
        if(i == n) return list;
    } else if (n < 0) {
        int i;
        list = ListGetLast(list);
        n = -n;
        for(i = 1; i <= n && list->prev != NULL; i++)
            list = list->prev;
        if(i == n) return list;
    }
    return 0;
};

struct ListElem * ListFindByData (ListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    while (list->next != NULL && list->Data != data)
        list = list->next;
    if(list->Data != data)
        return NULL;
    return list;
};

int ListPosition (ListItem *list, ListItem *elem) {
    if(!list || !elem) return NULL;
    int n;
    for (n = 1; list->next != 0 && elem != list; n++)
        list = list->next;
    if (list != elem)
        return -1;
    return n;
}
