#include "list.h"
#include <stdlib.h>
#include <malloc.h>
#define DATA_TYPE int

struct DListCElem * DListCCreate (DATA_TYPE data) {
    struct DListCElem *A = (struct DListCElem *) malloc(sizeof(struct DListCElem));
    if(!A) return NULL;
    A->Data = data;
    A->next = A;
    A->prev = A;
    return A;
}

struct DListCElem * DListCGetLast (DListCItem *list) {
    if(!list) return NULL;
    DListCItem *tmp = list;
    while (tmp->next != list)
        tmp = tmp->next;
    return list;
}

void DListCInsert (DListCItem *elem, DATA_TYPE data) {
    if(!elem) return;
    DListCItem *tmp_elem = DListCCreate(data);
    if (!tmp_elem) return;
    tmp_elem->prev = elem;
    elem->next->prev = tmp_elem;
    tmp_elem->next = elem->next;
    elem->next = tmp_elem;
    return;
}

DATA_TYPE DListCDeleteThisItem (DListCItem **elemlink) {
    if(!elemlink) return NULL;
    DListCItem *elem = *elemlink;
    if(!elem) return NULL;
    if(elem != elem->next) {
        DListCItem *new_link = NULL;
        elem->prev->next = elem->next;
        elem->next->prev = elem->prev;
        new_link = elem->next;
        *elemlink = new_link;
    }
    DATA_TYPE data = elem->Data;
    elem->Data = 0;
    elem->next = elem->prev = NULL;
    free(elem);
    return data;
}

DATA_TYPE DListCDeleteNextElem (DListCItem *elem) {
    if(!elem) return NULL;
    if (elem->next == elem) return NULL;
    elem = elem->next;
    return DListCDeleteThisItem(&elem);
}

DATA_TYPE DListCDeletePrevElem (DListCItem *elem) {
    if(!elem || elem->prev == elem) return NULL;
    DATA_TYPE data = elem->Data;
    elem->prev->Data = elem->Data;
    elem->prev->next = elem->next;
    elem->next->prev = elem->prev;
    free (elem);
    return data;
}

struct DListCElem * DListCRemoveByData (DListCItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListCItem *tmp = list;
    if(list->Data == data){
        DListCDeleteThisItem(&list);
    } else {
        while (tmp->next != NULL)
            if(tmp->next->Data != data)
                tmp = tmp->next;
            else {
                DListCDeleteNextElem(tmp);
                return list;
            }
    }
    return list;
}

struct DListCElem * DListCRemoveAllByData (DListCItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListCItem *tmp = list;
    while (tmp->next != list)
        if(tmp->Data != data)
                tmp = tmp->next;
        else {
            DListCDeleteThisItem(&tmp);
        }
    return tmp;
}

void DListCFree (DListCItem *list) {
    if(!list) return;
    while (list->next != list) {
        DListCItem *tmp = list;
        list = list->next;
        DListCDeleteThisItem(&tmp);
    }
//    DListCDeleteThisItem(list);
}

unsigned int DListCLengh (DListCItem *list) {
    if(!list) return 0;
    unsigned int l = 1;
    DListCItem *tmp = list;
    while (list != tmp->next) {
        tmp = tmp->next;
        l++;
    }
    return l;
}

struct DListCElem * DListCReverse (DListCItem * list) {
    if(!list) return NULL;
    DListCItem * list_tmp = list;
    DListCItem *tmp = list_tmp->next;
    list_tmp->next = list_tmp->prev;
    list_tmp->prev = tmp;
    while (list_tmp->prev != list) {
        list_tmp = list_tmp->prev;
        tmp = list_tmp->next;
        list_tmp->next = list_tmp->prev;
        list_tmp->prev = tmp;
    }
    return list;
}

struct DListCElem * DListCGetCopy (DListCItem * list) {
    if(!list) return NULL;
    DListCItem *old_list = list;
    DListCItem *new_list = DListCCreate(list->Data);
    DListCItem *tmp = new_list;
    while (old_list->next != list) {
        old_list = old_list->next;
        DListCInsert(tmp, list->Data);
        tmp = tmp->next;
    }
    return new_list;
}

struct DListCElem * DListCNth (DListCItem *list, int n) {
    if(!list) return NULL;
    DListCItem *gotelem = list;
    if (n > 0) {
        int i;
        for(i = 1; i <= n && list->next != gotelem; i++)
            list = list->next;
        if(i - 1 == n) return list;
    } else if (n < 0) {
        int i;
        list = DListCGetLast(list);
        n = -n;
        for(i = 1; i <= n && list->prev != gotelem; i++){
            list = list->prev;
        }
        if(i - 1 == n) return list;
    }
    return 0;
}

struct DListCElem * DListCFindByData (DListCItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListCItem *tmp = list;
    while (tmp->next != list && tmp->Data != data)
        tmp = tmp->next;
    if(tmp->Data != data)
        return NULL;
    return tmp;
}

struct DListCElem * DListCFindCustom (DListCItem *list, int (*compare_func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data) {
    if(!list) return NULL;
    DListCItem *tmp = list;
    while ((*compare_func)(data, tmp->Data) != 0 && tmp->next != list) {
        tmp = tmp->next;
    }
    if((*compare_func)(data, tmp->Data) != 0)
        return NULL;
    return tmp;
}

int DListCPosition (DListCItem *list, DListCItem *elem) {
    if(!list || !elem) return NULL;
    int n;
    DListCItem *tmp = list;
    for (n = 1; list != tmp->next && elem != tmp; n++)
        tmp = tmp->next;
    if (tmp != elem)
        return -1;
    return n - 1;
}

void DListCForeach (DListCItem *list, void (*func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data) {
    if(!list) return NULL;
    do {
        (*func)(list->Data, data);
        list = list->next;
    } while (list!= NULL);
}
