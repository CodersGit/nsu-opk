#ifndef LIST_H
#define LIST_H
#define DATA_TYPE int

typedef struct DListCElem {
    DATA_TYPE Data;
    struct DListCElem *next;
    struct DListCElem *prev;
} DListCItem;

struct DListCElem * DListCCreate (DATA_TYPE data) ;
struct DListCElem * DListCGetLast (DListCItem *list);
void DListCInsert (DListCItem *elem, DATA_TYPE data);
DATA_TYPE DListCDeleteThisItem (DListCItem **elemlink);
DATA_TYPE DListCDeleteNextElem (DListCItem *elem);
DATA_TYPE DListCDeletePrevElem (DListCItem *elem);
struct DListCElem * DListCRemoveByData (DListCItem *list, DATA_TYPE data);
struct DListCElem * DListCRemoveAllByData (DListCItem *list, DATA_TYPE data);
void DListCFree (DListCItem *list);
unsigned int DListCLengh (DListCItem *list);
struct DListCElem * DListCReverse (DListCItem * list);
struct DListCElem * DListCGetCopy (DListCItem * list);
struct DListCElem * DListCNth (DListCItem *list, int n);
struct DListCElem * DListCFindByData (DListCItem *list, DATA_TYPE data);
struct DListCElem * DListCFindCustom (DListCItem *list, int (*compare_func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data);
int DListCPosition (DListCItem *list, DListCItem *elem);
void DListCForeach (DListCItem *list, void (*func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data);
#endif // LIST_H
