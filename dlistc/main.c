#include <stdio.h>
#include <assert.h>
#include "list.h"

#define TEST_INIT\
    list = CreateTestList();\
    tmp = NULL;
#define GARBAGE_COLLECTOR\
    if (!tmp) DListCFree(tmp);\
    DListCFree(list);\
    tmp = NULL;\
    list = NULL;

int MakeTestCopmare (DATA_TYPE a, DATA_TYPE b) {
    return a != b;
}

void * CreateTestList () {
    void *list;
    assert((list = DListCCreate(0)));
    for (int i = 100; i >= 1; i--)
        DListCInsert(list, i);
    return list;
}

void TestDelitionAndCoping() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListCDeleteNextElem(list));
    assert(tmp = DListCFindByData(list, 50));
    assert(DListCDeletePrevElem(tmp));
    assert(DListCFindCustom(list, MakeTestCopmare, 30));
    assert(tmp = DListCGetCopy(list));
    GARBAGE_COLLECTOR
    return;
}

void TestTakeItLast() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListCLengh(DListCGetLast(list)) == 1);
    GARBAGE_COLLECTOR
    return;
}

void TestTakeItByNumber() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(tmp = DListCNth(list, 5));
    assert(DListCPosition(list, tmp) == 5);
    GARBAGE_COLLECTOR
    return;
}

void TestRemovingLenghAndReverse() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListCLengh(list) == 101);
    assert(DListCRemoveAllByData(list, 5));
    assert(DListCLengh(list) == 100);
    assert(DListCReverse(list));
    assert(DListCLengh(list) == 100);
    GARBAGE_COLLECTOR
    return;
}

int main() {
    TestDelitionAndCoping();
//    TestTakeItLast(); // In cycled list incorrect
    TestTakeItByNumber();
    TestRemovingLenghAndReverse();
    return 0;
}
