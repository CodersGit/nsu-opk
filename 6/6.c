#include <stdio.h>
#include <math.h>

int main() {
    int a, b, c, x1, x2;
    float d;
    printf ("You have something like that: aX^2 + bX + c = 0\na = ");
    if (!scanf("%d", &a) || (a == 0)) {
        printf("Incorrect input!");
        return 0;
    }
    printf("b = ");
    if (!scanf("%d", &b)) {
        printf("Incorrect input!");
        return 0;
    }
    printf("c = ");
    if (!scanf("%d", &c)) {
        printf("Incorrect input!");
        return 0;
    }
    d = (float) (b^2 - 4*a*c);
    if(d >= 0) {
        x1 = (-b + sqrt(d))/(2*a);
        x2 = (-b - sqrt(d))/(2*a);
        printf("x = %d; %d", x1, x2);
    } else printf ("Haven't solutions");
    return 0;
}
