#include <stdio.h>
#define ARRAY_SIZE 10

int main() {
    int a[ARRAY_SIZE] = {0,1,2,3,4,5,6,7,8,9};
    for(int i = 0; i < ARRAY_SIZE;i++)
        printf("%d, ", a[i]);
    printf("\n");
    reverse_array(a);
    for(int i = 0; i < ARRAY_SIZE;i++)
        printf("%d, ", a[i]);
    return 0;
}
void reverse_array (int a[ARRAY_SIZE]) {
    for(int i = 0; i <= ARRAY_SIZE / 2 - 1; i++){
        my_swap(a+i, a + ARRAY_SIZE - i - 1);
    }
}
void my_swap (int *b, int *c) {
        int tmp = *c;
        *c =*b;
        *b = tmp;
}
