#ifndef LIST_H
#define LIST_H
#define DATA_TYPE int

typedef struct DListElem {
    DATA_TYPE Data;
    struct DListElem *next;
    struct DListElem *prev;
} DListItem;

struct DListElem * DListCreate (DATA_TYPE data);
struct DListElem * DListGetLast (DListItem *list);
struct DListElem * DListAppend (DListItem *list, DATA_TYPE data);
struct DListElem * DListPrepend (DListItem *list, DATA_TYPE data);
void DListInsert (DListItem *elem, DATA_TYPE data);
DATA_TYPE DListDeleteThisItem (DListItem **elemlink);
DATA_TYPE DListDeleteNextElem (DListItem *elem);
DATA_TYPE DListDeletePrevElem (DListItem *elem);
struct DListElem * DListRemoveByData (DListItem *list, DATA_TYPE data);
struct DListElem * DListRemoveAllByData (DListItem *list, DATA_TYPE data);
void DListFree (DListItem *list);
unsigned int DListLengh (DListItem *list);
struct DListElem * DListReverse (DListItem * list);
struct DListElem * DListGetCopy (DListItem * list);
struct DListElem * DListNth (DListItem *list, int n);
struct DListElem * DListFindByData (DListItem *list, DATA_TYPE data);
struct DListElem * DListFindCustom (DListItem *list, int (*compare_func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data);
int DListPosition (DListItem *list, DListItem *elem);
void DListForeach (DListItem *list, void (*func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data);
#endif
