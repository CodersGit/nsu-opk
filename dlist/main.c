#include <stdio.h>
#include <assert.h>
#include "list.h"

#define TEST_INIT\
    list = CreateTestList();\
    tmp = NULL;
#define GARBAGE_COLLECTOR\
    if (!tmp) DListFree(tmp);\
    DListFree(list);\
    tmp = NULL;\
    list = NULL;

int MakeTestCopmare (DATA_TYPE a, DATA_TYPE b) {
    return a != b;
}

void * CreateTestList () {
    void *list;
    assert((list = DListCreate(0)));
    for (int i = 1; i <= 100; i++)
        assert(list = DListAppend(list, i));
    return list;
}
void TestDelitionAndCoping() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListDeleteNextElem(list));
    assert(tmp = DListFindByData(list, 50));
    assert(DListDeletePrevElem(tmp));
    assert(DListFindCustom(list, MakeTestCopmare, 30));
    assert(tmp = DListGetCopy(list));
    GARBAGE_COLLECTOR
    return;
}

void TestTakeItLast() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListLengh(DListGetLast(list)) == 1);
    GARBAGE_COLLECTOR
    return;
}

void TestTakeItByNumber() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(tmp = DListNth(list, 5));
    assert(DListPosition(list, tmp) == 5);
    GARBAGE_COLLECTOR
    return;
}

void TestRemovingLenghAndReverse() {
    void *list;
    void *tmp;
    TEST_INIT
    assert(DListLengh(list) == 101);
    assert(DListRemoveAllByData(list, 5));
    assert(DListLengh(list) == 100);
    assert(list = DListReverse(list));
    assert(DListLengh(list) == 100);
    GARBAGE_COLLECTOR
    return;
}

int main() {
    TestDelitionAndCoping();
    TestTakeItLast();
    TestTakeItByNumber();
    TestRemovingLenghAndReverse();
    return 0;
}
