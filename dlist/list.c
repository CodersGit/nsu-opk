#include "list.h"
#include <stdlib.h>
#include <malloc.h>

struct DListElem * DListCreate (DATA_TYPE data) {
    struct DListElem *A = (struct DListElem *) malloc(sizeof(struct DListElem));
    if(!A) return NULL;
    A->Data = data;
    A->next = NULL;
    A->prev = NULL;
    return A;
}

struct DListElem * DListGetLast (DListItem *list) {
    if(!list) return NULL;
    while (list->next != NULL)
        list = list->next;
    return list;
}

struct DListElem * DListAppend (DListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListItem *tmp_elem = DListGetLast(list);
    tmp_elem->next = DListCreate(data);
    if (!tmp_elem->next) return NULL;
    tmp_elem->next->prev = tmp_elem;
    return list;
}

struct DListElem * DListPrepend (DListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListItem *tmp_elem = list;
    tmp_elem->prev = DListCreate(data);
    if (!tmp_elem->prev) return NULL;
    tmp_elem->prev->next = tmp_elem;
    tmp_elem = tmp_elem->prev;
    return tmp_elem;
}

void DListInsertAfter (DListItem *elem, DATA_TYPE data) {
    if(!elem) return;
    DListItem *tmp_elem = DListCreate(data);
    if (!tmp_elem) return;
    tmp_elem->prev = elem;
    if(elem->next != NULL) {
        elem->next->prev = tmp_elem;
        tmp_elem->next = elem->next;
    }
    elem->next = tmp_elem;
    return;
}

void DListInsertBefore (DListItem *elem, DATA_TYPE data) {
    if(!elem) return;
    DListItem *tmp_elem = DListCreate(data);
    if (!tmp_elem) return;
    tmp_elem->next = elem;
    if(elem->prev != NULL) {
        elem->prev->next = tmp_elem;
        tmp_elem->prev = elem->prev;
    }
    elem->prev = tmp_elem;
    return;
}

DATA_TYPE DListDeleteThisItem (DListItem **elemlink) {
    if(!elemlink) return NULL;
    DListItem *elem = *elemlink;
    if(!elem) return NULL;
    DListItem *new_link = NULL;
    if(!elem) return NULL;
    if (elem->prev != NULL) {
        elem->prev->next = elem->next;
    }
    if (elem->next != NULL) {
        elem->next->prev = elem->prev;
        new_link = elem->next;
    }
    DATA_TYPE data = elem->Data;
    elem->Data = 0;
    elem->next = elem->prev = NULL;
    free(elem);
    *elemlink = new_link;
    return data;
}

DATA_TYPE DListDeleteNextElem (DListItem *elem) {
    if(!elem) return NULL;
    if (elem->next == NULL) return NULL;
    elem = elem->next;
    return DListDeleteThisItem(&elem);
}

DATA_TYPE DListDeletePrevElem (DListItem *elem) {
    if(!elem || !elem->prev) return NULL;
    DATA_TYPE data = elem->Data;
    elem->prev->Data = elem->Data;
    elem->prev->next = elem->next;
    elem->next->prev = elem->prev;
    free (elem);
    return data;
}

struct DListElem * DListRemoveByData (DListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListItem *tmp = list;
    if(list->Data == data){
        DListDeleteThisItem(&list);
    } else {
        while (tmp->next != NULL)
            if(tmp->next->Data != data)
                tmp = tmp->next;
            else {
                DListDeleteNextElem(tmp);
                return list;
            }
    }
    return list;
}

struct DListElem * DListRemoveAllByData (DListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    DListItem *tmp = list;
    while (tmp->next != NULL)
        if(tmp->Data != data)
                tmp = tmp->next;
        else {
            DListDeleteThisItem(&tmp);
        }
    return tmp;
}

void DListFree (DListItem *list) {
    if(!list) return;
    while (list->next != NULL) {
        DListItem *tmp = list;
        list = list->next;
        DListDeleteThisItem(&tmp);
    }
}

unsigned int DListLengh (DListItem *list) {
    if(!list) return 0;
    unsigned int l = 1;
    while (list->next != NULL) {
        list = list->next;
        l++;
    }
    return l;
}

struct DListElem * DListReverse (DListItem * list) {
    if(!list) return NULL;
    DListItem *tmp = list->next;
    list->next = list->prev;
    list->prev = tmp;
    while (list->prev != NULL) {
        list = list->prev;
        tmp = list->next;
        list->next = list->prev;
        list->prev = tmp;
    }
    return list;
}

struct DListElem * DListGetCopy (DListItem * list) {
    if(!list) return NULL;
    DListItem *new_list = DListCreate(list->Data);
    DListItem *tmp = new_list;
    while (list->next != NULL) {
        list = list->next;
        DListInsertAfter(tmp, list->Data);
        tmp = tmp->next;
    }
    return new_list;
}

struct DListElem * DListNth (DListItem *list, int n) {
    if(!list) return NULL;
    if (n > 0) {
        int i;
        for(i = 1; i <= n && list->next != NULL; i++)
            list = list->next;
        if(i - 1 == n) return list;
    } else if (n < 0) {
        int i;
        list = DListGetLast(list);
        n = -n;
        for(i = 1; i <= n && list->prev != NULL; i++){
            list = list->prev;
        }
        if(i - 1 == n) return list;
    }
    return 0;
}

struct DListElem * DListFindByData (DListItem *list, DATA_TYPE data) {
    if(!list) return NULL;
    while (list->next != NULL && list->Data != data)
        list = list->next;
    if(list->Data != data)
        return NULL;
    return list;
}

struct DListElem * DListFindCustom (DListItem *list, int (*compare_func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data) {
    if(!list) return NULL;
    while ((*compare_func)(data, list->Data) != 0 && list->next != NULL) {
        list = list->next;
    }
    if((*compare_func)(data, list->Data) != 0)
        return NULL;
    return list;
}

int DListPosition (DListItem *list, DListItem *elem) {
    if(!list || !elem) return NULL;
    int n;
    for (n = 1; list->next != 0 && elem != list; n++)
        list = list->next;
    if (list != elem)
        return -1;
    return n - 1;
}

void DListForeach (DListItem *list, void (*func)(DATA_TYPE a, DATA_TYPE b), DATA_TYPE data) {
    if(!list) return NULL;
    do {
        (*func)(list->Data, data);
        list = list->next;
    } while (list!= NULL);
}
