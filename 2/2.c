#include <stdio.h>
#include <stdbool.h>

int main () {
    int count = 0;
    for (int i = 0; i < 1000000; i++)
        if (CheckHappiness(i))
            count++;
    printf("\"Happy\" tickets %d / 1000000 %d percentage", count, count/10000);
    return 0;
}
int CheckHappiness (int number) {
    if (NumSumm(number%1000) == NumSumm(number/1000)){
 //       printf("%d\n", number);
        return true;
    }
    return false;
}
int NumSumm (int number) {
    int result = 0;
    for (;number > 0; number /= 10) {
        result += number %10;
    }
    return result;
}
