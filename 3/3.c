#include <stdio.h>
#include <stdbool.h>

int main () {
    while (1) {
        printf("Input what to do:\n0 to exit\n1 to port F -> C\n2 to port C -> F\n");
        int whattodo;
        float c, f;
        if (!scanf("%d", &whattodo)) {
            char i;
            scanf("%s", &i);
            whattodo = 3;
        }
        switch (whattodo) {
            case 0: return 0; break;
            case 1:
                printf("Input temperature in F: ");
                if(!scanf("%f", &f)) {
                    char i;
                    scanf("%s", &i);
                    printf("Incorrect input!\n\n");
                    continue;
                }
                c =  (f - 32) * 5/9;
                printf("Result in C is %f\n\n", c);
                break;
            case 2:
                printf("Input temperature in C: ");
                if(!scanf("%f", &c)) {
                    char i;
                    scanf("%s", &i);
                    printf("Incorrect input!\n\n");
                    continue;
                }
                f =  c * 9/5 + 32;
                printf("Result in F is %f\n\n", f);
                break;
            default:
                printf("Incorrect action!\n\n");
                break;
        }
    }
    return 0;
}

